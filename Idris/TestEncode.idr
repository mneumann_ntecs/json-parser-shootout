module Main

import Json.Data
import Json.Encode

jsonVal : JValue
jsonVal = JArray [JString "hallo", JNull, JNumber 1.2, JArray []]

main : IO ()
main = putStrLn (encodeJson jsonVal)
