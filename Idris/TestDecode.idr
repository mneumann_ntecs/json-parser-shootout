module Main

import Json.Data
import Json.Encode
import Json.Decode

jsonStr : String
jsonStr = "[null, 123.3]" 

decode : String -> String
decode str = case decodeJson str of
                   Just json => "Success: " ++ (encodeJson json)
                   Nothing => "Failed to decode Json: " ++ str

main : IO ()
main = putStrLn (decode jsonStr)
