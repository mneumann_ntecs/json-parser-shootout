module Json.Encode

import Json.Data

total
join : String -> List String -> String
join sep [] = ""
join sep (x :: []) = x
join sep (x :: (y :: xs)) = x ++ sep ++ (join sep (y::xs))

testJoinEmptyList : (join "," []) = ""
testJoinEmptyList = Refl

testJoinListWithOneElement : (join "," ["a"]) = "a"
testJoinListWithOneElement = Refl

testJoinList : (join "," ["a", "b", "c"]) = "a,b,c"
testJoinList = Refl

escapeString : String -> String
escapeString x = x -- TODO

export
encodeJson : JValue -> String
encodeJson JNull = "null"
encodeJson (JBool False) = "false"
encodeJson (JBool True) = "true"
encodeJson (JNumber x) = show x
encodeJson (JString x) = "\"" ++ escapeString x ++ "\""
encodeJson (JArray xs) = "[" ++ join ", " (map encodeJson xs) ++ "]"
encodeJson (JObject xs) = "{" ++ join ", " (map (\(k, v) => show k ++ ": " ++ encodeJson v) xs) ++ "}"
