module Json.Decode

import Data.String
import Json.Data

total
parseNull : List Char -> Either ((), List Char) ()
parseNull ('n' :: 'u' :: 'l' :: 'l' :: xs) = Left ((), xs)
parseNull xs = Right ()

testParseNull : parseNull (unpack "null,123") = Left ((), unpack ",123")
testParseNull = Refl

total
parseBool : List Char -> Either (Bool, List Char) ()
parseBool ('t' :: 'r' :: 'u' :: 'e' :: xs) = Left (True, xs)
parseBool ('f' :: 'a' :: 'l' :: 's' :: 'e' :: xs) = Left (False, xs)
parseBool xs = Right ()

testParseBoolTrue : parseBool (unpack "true,") = Left (True, unpack ",")
testParseBoolTrue = Refl
testParseBoolFalse : parseBool (unpack "false,") = Left (False, unpack ",")
testParseBoolFalse = Refl

endOfString : Char -> Bool
endOfString '"' = True
endOfString _ = False

isFirstOfNumber : Char -> Bool
isFirstOfNumber '+' = True
isFirstOfNumber '-' = True
isFirstOfNumber c = isDigit c

total
parseNumber : List Char -> Either (Double, List Char) ()
parseNumber xs = case (List.span spanNumber xs) of
                       (numberChars, post) => case String.parseDouble (pack numberChars) of
                                                  Just double => Left (double, post)
                                                  _ => Right ()
where
  spanNumber : Char -> Bool
  spanNumber '+' = True
  spanNumber '-' = True
  spanNumber 'e' = True
  spanNumber 'E' = True
  spanNumber '.' = True
  spanNumber c = isDigit c

total
parseString : List Char -> Either (String, List Char) ()
parseString ('"' :: xs) = case (List.span inString xs) of
                                (chars, '"' :: post) => Left ((pack chars), post) 
                                _ => Right ()
where
  inString : Char -> Bool
  inString c = not (endOfString c)

parseString xs = Right ()

parseJValue : List Char -> Either (JValue, List Char) ()

parseList : List Char -> Either ((List JValue), List Char) ()
parseObject : List Char -> Either ((List (String, JValue)), List Char) ()

total
mapLeft : (a -> b) -> Either (a, List Char) () -> Either (b, List Char) ()
mapLeft f (Left (x, rem)) = Left (f x, rem)
mapLeft f (Right r) = Right r

skipWhite : List Char -> List Char
skipWhite [] = []
skipWhite (c :: xs) = if isSpace c then skipWhite xs else c :: xs

testSkipWhite : skipWhite (unpack "f") = ['f'] 
testSkipWhite = Refl

testSkipWhite' : skipWhite (unpack "    f") = ['f'] 
testSkipWhite' = Refl

parseList ('[' :: xs) =
  case skipWhite xs of
       -- We got an empty list
       (']' :: xs') => Left ([], xs') 
       -- non empty list
       (xs') => parseListMore xs'
where
  -- parses one or more JValue's
  parseListMore : List Char -> Either (List JValue, List Char) ()
  parseListMore xs =
     case parseJValue (skipWhite xs) of
             Left (value, rem) => case (skipWhite rem) of
                                      (']' :: rem') => Left(value :: [], rem') 
                                      (',' :: rem') => mapLeft (\lst => (value :: lst)) (parseListMore rem')
                                      _ => Right ()
             Right () => Right ()
parseList _ = Right () 

parseObject ('{' :: xs) =
  case skipWhite xs of
       -- We got an empty object
       ('}' :: xs') => Left ([], xs') 
       -- non empty object
       (xs') => parseObjectMore xs'
where
  -- parses one or more key/value tuples 
  parseObjectMore : List Char -> Either (List (String, JValue), List Char) ()
  parseObjectMore xs =
     case parseString (skipWhite xs) of
          Left (key, remKey) =>
              case skipWhite remKey of
                  (':' :: remSep) =>
                      case parseJValue (skipWhite remSep) of
                          Left (value, remVal) =>
                              case (skipWhite remVal) of
                                  ('}' :: remLst) => Left((key, value) :: [], remLst) 
                                  (',' :: remItm) => mapLeft (\lst => ((key, value) :: lst)) (parseObjectMore remItm)
                                  _ => Right ()
                          Right () => Right ()
                  _ => Right ()  
          Right () => Right ()

parseObject _ = Right () 

parseJValue [] = Right ()
parseJValue ('n' :: xs) = mapLeft (\_ => JNull) (parseNull ('n' :: xs))
parseJValue ('t' :: xs) = mapLeft (\v => JBool v) (parseBool ('t' :: xs))
parseJValue ('f' :: xs) = mapLeft (\v => JBool v) (parseBool ('f' :: xs))
parseJValue ('"' :: xs) = mapLeft (\v => JString v) (parseString ('"' :: xs))
parseJValue ('[' :: xs) = mapLeft (\v => JArray v) (parseList ('[' :: xs))
parseJValue ('{' :: xs) = mapLeft (\v => JObject v) (parseObject ('{' :: xs))
parseJValue (x :: xs) = if isSpace x then parseJValue xs
                        else if isFirstOfNumber x then mapLeft (\v => JNumber v) (parseNumber (x :: xs))
                        else Right ()

export
decodeJson: String -> Maybe JValue
decodeJson s =
  case parseJValue (skipWhite (unpack s)) of
    Left (value, rem) =>
      case (skipWhite rem) of
           [] => Just value
           _ => Nothing
    Right () => Nothing
