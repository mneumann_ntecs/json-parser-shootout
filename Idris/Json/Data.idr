module Json.Data

public export
data JValue = JNull
            | JBool Bool
            | JNumber Double
            | JString String
            | JArray (List JValue)
            | JObject (List (String, JValue))
